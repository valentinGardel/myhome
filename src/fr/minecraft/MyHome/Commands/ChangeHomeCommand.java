package fr.minecraft.MyHome.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.TabCompleters.HomeTabCompleter;
import fr.minecraft.MyHome.Utils.Code;

public class ChangeHomeCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "chghome";
		TAB_COMPLETER = HomeTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			if(args.length==1)
				this.changeHomeMessage((Player) sender, args[0]);
			else if(args.length==2 && args[1].equalsIgnoreCase("confirm"))
				this.changeHome((Player) sender, args[0]);
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible pour un non-joueur!");
		return true;
	}
	
	private void changeHomeMessage(Player sender, String name)
	{
		MessageBuilder.create(ChatColor.GOLD+"�tes-vous sur de vouloir modifier le home \""+name+"\"?")
			.onClickRunCommand("/chghome "+name+" confirm").onHoverText("Valider").write(ChatColor.DARK_GREEN+" [Valider]")
			.send(sender);
	}

	/**
	 * fonction pour modifier un home
	 * @param player est le joueur de qui on veut modifier le home
	 * @param name est le nom du home a modifier
	 */
	public void changeHome(Player player, String name)
	{
		Home home = Home.GetHome(player.getUniqueId(), name);
		if(home != null)
		{
			if(Home.DeleteHome(player.getUniqueId(), name))
			{
				Code code = Home.SaveHome(player, name, player.getLocation());
				switch(code)
				{
				case Success:
					player.sendMessage(ChatColor.GREEN+"Le home \""+ name + "\" a bien �t� modifi�!");
					break;
				case TooMany:
					player.sendMessage(ChatColor.RED+"Vous avez atteint le nombre de home autoris�, vous devez en supprimer pour pouvoir en ajouter d'autre!");
					break;
				default:
					player.sendMessage(ChatColor.RED+"Erreur!");
					break;
				}
			}
			else
				player.sendMessage(ChatColor.RED+"Le changement du home a �chou�!");
		}
		else
			player.sendMessage(ChatColor.RED+"Le home \""+ name + "\" n'existe pas!");
	}
}
