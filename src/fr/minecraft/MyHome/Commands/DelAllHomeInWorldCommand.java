package fr.minecraft.MyHome.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyHome.Models.Home;

public class DelAllHomeInWorldCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "delallhomeinworld";
		TAB_COMPLETER = null;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length==1)
		{
			World world = Bukkit.getWorld(args[0]);
			if(world!=null)
			{
				if(Bukkit.getWorld(args[0])!=null && Home.DeleteHomesInWorld(world.getUID().toString()))
					sender.sendMessage(ChatColor.GREEN+"Tout les homes du monde "+args[0]+" ont �t� supprim�s");
				else
					sender.sendMessage(ChatColor.RED+"Impossible de supprimer les homes du monde "+args[0]);
			}
			else
				sender.sendMessage(ChatColor.RED+"Monde "+args[0]+" introuvable");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		
		return true;
	}

}
