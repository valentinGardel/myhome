package fr.minecraft.MyHome.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.Utils.Code;

public class SetHomeCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "sethome";
		TAB_COMPLETER = null;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			if(args.length==1)
				this.setHome((Player) sender, args[0]);
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible pour un non-joueur!");
		return true;
	}

	/**
	 * fonction qui permet de definir un home
	 * @param player est le joueur a qui on veut definir le home
	 * @param name est le nom que le joueur veut donner a son home
	 */
	public void setHome(Player player, String name)
	{
		if (Home.GetHome(player.getUniqueId(), name)==null)
		{
			Code code = Home.SaveHome(player, name, player.getLocation());
			switch(code)
			{
			case Success:
				player.sendMessage(ChatColor.GREEN+"Le home \""+ name + "\" a bien �t� ajout�!");
				break;
			case TooMany:
				player.sendMessage(ChatColor.RED+"Vous avez atteint le nombre de home autoris�, vous devez en supprimer pour pouvoir en ajouter d'autre!");
				break;
			default:
				player.sendMessage(ChatColor.RED+"Erreur!");
				break;
			}
		}
		else
			player.sendMessage(ChatColor.RED+"Le home \""+name+"\" existe d�j�!");
	}
}
