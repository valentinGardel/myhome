package fr.minecraft.MyHome.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.TabCompleters.HomeTabCompleter;

public class DelHomeCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "delhome";
		TAB_COMPLETER = HomeTabCompleter.class;
	}
	
	/**
	 * Fonction appel� lorsque la commande "home" est utilise ou un de ces aliases
	 */
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(args.length == 1)
				this.delHome(player, args[0]);
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible pour un non-joueur!");
		return true;
	}
	
	/**
	 * fonction pour supprimer un home
	 * @param player est le joueur a qui on veut supprimer le home
	 * @param name est le nom du home a supprimer
	 */
	public void delHome(Player player, String name)
	{
		if(Home.GetHome(player.getUniqueId(), name) == null)
			player.sendMessage(ChatColor.RED+"Le home "+ name + " n'existe pas");
		else
		{
			if(Home.DeleteHome(player.getUniqueId(), name))
				player.sendMessage(ChatColor.GREEN+"home "+ name +" supprim�");
			else
				player.sendMessage(ChatColor.RED+"La suppression du home a �chou�");
		}
	}
}
