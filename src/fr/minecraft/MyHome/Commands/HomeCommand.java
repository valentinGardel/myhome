package fr.minecraft.MyHome.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.TabCompleters.HomeTabCompleter;

public class HomeCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "home";
		TAB_COMPLETER = HomeTabCompleter.class;
	}
	
	/**
	 * Fonction appel� lorsque la commande "home" est utilise ou un de ces aliases
	 */
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if(args.length == 0)
				this.displayHomes(player);
			else if(args.length == 1)
				this.homeTp(player, args[0]);
			else
				player.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible pour un non-joueur!");
		return true;
	}

	/**
	 * Permet d'afficher les homes du joueur
	 * @param player joueur � qui on veut afficher ses homes
	 * */
	private void displayHomes(Player player)
	{
		ArrayList<Home> homes = Home.GetHomes(player.getUniqueId());

		if(homes.size()>0)
		{
			player.sendMessage(ChatColor.GOLD+"Vous avez "+homes.size()+" home(s):");
			for(Home h: homes)
			{
				MessageBuilder.create().onHoverText(h.toString()).write(" - "+h.nom).write(" ")
					.onHoverText("Se t�l�porter").onClickRunCommand("/home "+h.nom).write(ChatColor.DARK_GREEN+"[TP]").write(" ")
					.onHoverText("Supprimer").onClickSugestCommand("/delhome "+h.nom).write(ChatColor.DARK_RED+"[X]")
					.send(player);
			}
		}
		else
			player.sendMessage(ChatColor.GOLD+"Vous n'avez aucun home");
	}

	
	/**
	 * fonction pour teleporter un joueur a son home
	 * @param player est le joueur qu on veut teleporter
	 * @param name nom du home
	 */
	public void homeTp(Player player, String name)
	{
		Home home = Home.GetHome(player.getUniqueId(), name);

		if(home != null)
		{
			Location location = home.getLocation();
			if(location!=null)
				player.teleport(location);
			else
				player.sendMessage(ChatColor.RED+"Impossible de se t�l�porter, la localisation du home est invalide");
		}
		else
			player.sendMessage(ChatColor.RED+"Le home "+ name + " n'existe pas");			
	}

	/**
	 * permet de teleporter un joueur au home d'un autre
	 * @param sender joueur a t�l�porter
	 * @param targetName nom du joueur dont on veut utilis� le home
	 * @param homeName nom du home � utiliser
	 * */
	public void homeTpOf(Player sender, String targetName, String homeName)
	{
		Player target = Bukkit.getPlayer(targetName);

		if(target!=null)
		{
			Home home = Home.GetHome(target.getUniqueId(), homeName);

			if(home!=null)
			{
				Location location = home.getLocation();
				if(location!=null)
					sender.teleport(location);
				else
					sender.sendMessage(ChatColor.RED+"Impossible de se t�l�porter, la localisation du home est invalide");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le home "+homeName+" du joueur "+targetName+" n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+targetName+" est introuvable");
	}
}
