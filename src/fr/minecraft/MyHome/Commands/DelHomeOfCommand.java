package fr.minecraft.MyHome.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.TabCompleters.HomeTabCompleter;

public class DelHomeOfCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "delhomeof";
		TAB_COMPLETER = HomeTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length>0)
		{
			@SuppressWarnings("deprecation")
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
			if(player != null)
			{
				if(args.length==1)
					this.delHomesOf(sender, player);
				else if(args.length==2)
					this.delHomeOf(sender, player, args[1]);
				else
					sender.sendMessage(ChatColor.RED+"Commande invalide");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}

	private void delHomeOf(CommandSender sender, OfflinePlayer player, String name)
	{
		if(Home.DeleteHome(player.getUniqueId(), name))
			sender.sendMessage(ChatColor.GREEN+"Le home \""+name+"\" du joueur "+player.getName()+" a �t� supprim�");
		else
			sender.sendMessage(ChatColor.RED+"Impossible de supprimer le home");
	}

	private void delHomesOf(CommandSender sender, OfflinePlayer player)
	{
		if(Home.DeleteHomes(player.getUniqueId()))
			sender.sendMessage(ChatColor.GREEN+"Les homes du joueur "+player.getName()+" ont �t� supprim�s");
		else
			sender.sendMessage(ChatColor.RED+"Impossible de supprimer les homes");
	}
	
	
}
