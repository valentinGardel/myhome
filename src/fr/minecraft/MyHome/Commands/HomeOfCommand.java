package fr.minecraft.MyHome.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyHome.Models.Home;
import fr.minecraft.MyHome.TabCompleters.HomeOfTabCompleter;
import fr.minecraft.MyHome.Utils.Permission;

/**
 * Command qui permet de voir les homes des autres joueurs
 * */
public class HomeOfCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "homeof";
		TAB_COMPLETER = HomeOfTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length>0)
		{
			@SuppressWarnings("deprecation")
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
			
			if(player != null && player.hasPlayedBefore())
			{
				if(args.length==1)
					this.displayHomes(sender, player);
				else if(sender instanceof Player)
					this.homeTp((Player) sender, player, args[1]);
				else
					sender.sendMessage(ChatColor.RED+"Commande invalide!");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" est introuvable");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
		return true;
	}
	
	public void homeTp(Player sender, OfflinePlayer player, String name)
	{
		if(sender.hasPermission(Permission.USE_OTHERS_HOME))
		{
			Home home = Home.GetHome(player.getUniqueId(), name);
	
			if(home != null)
			{
				Location location = home.getLocation();
				if(location!=null)
					sender.teleport(location);
				else
					sender.sendMessage(ChatColor.RED+"Impossible de se tÚlÚporter, la localisation du home est invalide");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le home "+ name + " n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Permission denied");
	}
	
	private void displayHomes(CommandSender sender, OfflinePlayer player)
	{
		ArrayList<Home> homes = Home.GetHomes(player.getUniqueId());
		
		sender.sendMessage(ChatColor.GOLD+"Le joueur "+player.getName()+" Ó "+homes.size()+" home(s):");
		for(Home home: homes)
		{
			MessageBuilder.create()
				.onHoverText(home.toString()).write(" - "+home.nom).write(" ")
				.onHoverText("Se tÚlÚporter").onClickRunCommand("/homeof "+player.getName()+" "+home.nom).write(ChatColor.DARK_GREEN+"[TÚlÚportation]").write(" ")
				.onHoverText("Supprimer").onClickSugestCommand("/delhomeof "+player.getName()+" "+home.nom).write(ChatColor.DARK_RED+"[Supprimer]")
				.send(sender);
		}
	}
}
