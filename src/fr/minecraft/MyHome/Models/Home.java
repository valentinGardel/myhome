package fr.minecraft.MyHome.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import ActiveRecord.Model;
import Commons.Logger;
import fr.minecraft.MyHome.Config;
import fr.minecraft.MyHome.Utils.Code;
import fr.minecraft.MyHome.Utils.Permission;

/** 
 * class qui represente un home 
 * */
public class Home extends Model
{
	protected static final String TABLE_NAME="Home";
	
	/** propietaire du home */
	public UUID owner;
	
	/** 
	 * nom: nom du home
	 * world: UID du monde ou se trouve le home
	 * */
	public String nom, world;
	
	/** coordonnees du home */
	public double x, y, z;
	
	/** direction **/
	public float pitch, yaw;
	
	/**
	 * Constructeur Home
	 * @param pOwner UUID du proprietaire du home
	 * @param pNom nom du home
	 * @param pL location du home (world, x, y, z)
	 * **/
	public Home(UUID pOwner, String pNom, Location pL) throws SQLException, NullPointerException
	{
		this.owner = pOwner;
		this.nom = pNom;
		this.x = pL.getX();
		this.y = pL.getY();
		this.z = pL.getZ();
		this.pitch = pL.getPitch();
		this.yaw = pL.getYaw();
		this.world = pL.getWorld().getName();
	}

	private Home(UUID pOwner, String pNom, String pWorld, double px, double py, double pz, float ppitch, float pyaw)
	{
		this.owner = pOwner;
		this.nom = pNom;
		this.x = px;
		this.y = py;
		this.z = pz;
		this.pitch = ppitch;
		this.yaw = pyaw;
		this.world = pWorld;
	}

	/** 
	 * Create the tables if needed 
	 * @throws SQLException 
	 * */
	public static void CreateTable() throws SQLException
	{
		Statement stmt = Config.GetConfig().getConnection().createStatement();
		stmt.executeUpdate("create table if not exists "+TABLE_NAME+"(playerUUID varchar(50), name varchar(20), x double(11,2), y double(11,2), z double(11,2), pitch double(11,2), yaw double(11,2), world varchar(50), primary key(playerUUID, name));");
		stmt.close();
	}

	/**
	 * @return localisation minecraft du home, null si le monde est introuvable
	 * */
	public Location getLocation()
	{
		World world=null;
		try {
			UUID uuid=UUID.fromString(this.world);
			world = Bukkit.getWorld(uuid);
		} catch(IllegalArgumentException ex) {
			Logger.Exception(ex);
			world = Bukkit.getWorld(this.world);
		}

		if(world==null)
			return null;

		return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
	}

	/**
	 * Retourne un string a afficher au joueur pour descrire un home
	 * Si le monde est introuvable le precise en rouge
	 * */
	public String toString()
	{
		Location location = this.getLocation();
		if(location==null)
		{
			return ChatColor.RED+this.nom+" ("+(int)this.x+", "+ (int)this.y+", "+(int)this.z+"), /!\\ monde introuvable!";
		}
		else
			return this.nom+" ("+(int)this.x+", "+ (int)this.y+", "+(int)this.z+"), "+location.getWorld().getName();
	}

	/**
	 * Permet d'avoir tout les homes d'un joueur depuis la bdd
	 * @param playerUUID uuid du joueur de qui on cherche les homes
	 * @return la liste de tout les homes du joueur
	 * */
	public static ArrayList<Home> GetHomes(UUID playerUUID)
	{
		ArrayList<Home> homes = new ArrayList<Home>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select name, x, y, z, pitch, yaw, world from "+TABLE_NAME+" where playerUUID=?;");
			st.setString(1, playerUUID.toString());
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				try {
					homes.add(new Home(
							playerUUID, 
							res.getString("name"), 
							res.getString("world"),
							res.getDouble("x"),
							res.getDouble("y"),
							res.getDouble("z"),
							res.getFloat("pitch"),
							res.getFloat("yaw")
							));
				} catch(NullPointerException e) {}
			}
		} catch (SQLException e) {
			Logger.Exception(e);
		}
		return homes;
	}

	/**
	 * Permet d'avoir un home specifique d'un joueur depuis la bdd
	 * @param playerUUID playerUUID uuid du joueur de qui on cherche le home
	 * @param homeName nom du home � chercher
	 * @return le home du joueur
	 * */
	public static Home GetHome(UUID playerUUID, String homeName)
	{
		Home home = null;
		try {
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("Select x, y, z, pitch, yaw, world from "+TABLE_NAME+" where playerUUID=? and name=?;");
			st.setString(1, playerUUID.toString());
			st.setString(2, homeName);
			ResultSet res = st.executeQuery();
			if(res.next())
			{
				try {
					home = new Home(
							playerUUID, 
							homeName, 
							res.getString("world"), 
							res.getDouble("x"), 
							res.getDouble("y"), 
							res.getDouble("z"),
							res.getFloat("pitch"),
							res.getFloat("yaw")
							);
				}catch(NullPointerException e) {}
			} 
		} catch (SQLException e) {
			Logger.Exception(e);
		}
		return home;
	}

	/**
	 * Permet d'ajouter un home dans la bdd
	 * @param playerUUID uuid du joueur a qui ajouter le home
	 * @param homeName nom a donner au home
	 * @param location du home a ajouter
	 * @return true si le home a bien ete ajouter, sinon false
	 * */
	public static Code SaveHome(Player player, String homeName, Location location)
	{
		int amountHome = Home.GetHomes(player.getUniqueId()).size();
		int maxHomeAllowed = Home.GetMaxHomeAllowed(player);

		if(amountHome < maxHomeAllowed)
		{
			PreparedStatement st;
			try {
				st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_NAME+" (playerUUID, name, x, y, z, pitch, yaw, world) values(?, ?, ?, ?, ?, ?, ?, ?);");
				st.setString(1, player.getUniqueId().toString());
				st.setString(2, homeName);
				st.setDouble(3, location.getX());
				st.setDouble(4, location.getY());
				st.setDouble(5, location.getZ());
				st.setFloat(6, location.getPitch());
				st.setFloat(7, location.getYaw());
				st.setString(8, location.getWorld().getUID().toString());
				st.executeUpdate();
			} catch (SQLException e) {
				Logger.Exception(e);
				return Code.Failed;
			}
			return Code.Success;
		}
		else
			return Code.TooMany;
	}

	/**
	 * Check les permissions pour trouver le nombre max de homes autoris�
	 * @param player dont on check les perms
	 * @return le nombre de home max autoris�
	 * */
	private static int GetMaxHomeAllowed(Player player)
	{
		if(player.hasPermission(Permission.INFINITE_HOME))
			return Integer.MAX_VALUE;

		int max=0;
		HashMap<Integer, String> rights = Config.GetConfig().getMaxHomeRights();
		for(Integer i : rights.keySet())
		{
			if(player.hasPermission(rights.get(i)) && i>max)
				max=i;
		}
		return max;
	}

	/**
	 * Permet de supprimer un home dans la bdd
	 * @param playerUUID uuid du joueur dont on supprime le home
	 * @param homeName nom du home a supprimer
	 * @return true si le home a bien ete supprime, sinon false
	 * */
	public static boolean DeleteHome(UUID playerUUID, String homeName)
	{
		boolean res=false;
		PreparedStatement st;
		try {
			st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_NAME+" where playerUUID=? and name=?;");
			st.setString(1, playerUUID.toString());
			st.setString(2, homeName);
			st.executeUpdate();
			res=true;
		} catch (SQLException e) {
			res=false;
			Logger.Exception(e);
		}
		return res;
	}

	/**
	 * Permet de supprimer tout les homes dans un monde
	 * @param world nom du monde o� supprimer les homes
	 * */
	public static boolean DeleteHomesInWorld(String world)
	{
		boolean res=false;
		PreparedStatement st;
		try {
			st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_NAME+" where world=?;");
			st.setString(1, world);
			st.executeUpdate();
			res=true;
		} catch (SQLException e) {
			res=false;
			Logger.Exception(e);
		}
		return res;
	}

	/**
	 * permet de supprimer tout les homes d'un joueur
	 * @param playerUUID uuid du joueur dont on supprimer les homes
	 * */
	public static boolean DeleteHomes(UUID playerUUID)
	{
		boolean res=false;
		PreparedStatement st;
		try {
			st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_NAME+" where playerUUID=?;");
			st.setString(1, playerUUID.toString());
			st.executeUpdate();
			res=true;
		} catch (SQLException e) {
			res=false;
			Logger.Exception(e);
		}
		return res;
	}
}
