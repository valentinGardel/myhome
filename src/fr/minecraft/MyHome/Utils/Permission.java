package fr.minecraft.MyHome.Utils;

public class Permission {
	public static final String
		USE_OTHERS_HOME="MyHome.canTpOtherPlayerHome",
		DEL_OTHERS_HOME="MyHome.canDelOtherPlayerHome",
		INFINITE_HOME="MyHome.infiniteHome";
}
