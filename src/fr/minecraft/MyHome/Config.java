package fr.minecraft.MyHome;

import Commons.DatabaseConfig;
import java.util.HashMap;
import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

public class Config extends DatabaseConfig
{
	private static Config instance;
	
	@Override
	public void initConfig(JavaPlugin plugin)
	{
		super.initConfig(plugin);
		Config.instance = this;
	}
	
	public static Config GetConfig()
	{
		return Config.instance;
	}
	
	public HashMap<Integer, String> getMaxHomeRights()
	{
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		List<String> rights = this.plugin.getConfig().getStringList("maxHomeRights");
		for(String value : rights)
		{
			String[] split = value.split(":");
			map.put(Integer.parseInt(split[0].trim()), split[1].trim());
		}
		return map;
	}
}
