package fr.minecraft.MyHome;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;

import Commons.MinecraftPlugin;
import fr.minecraft.MyHome.Commands.*;
import fr.minecraft.MyHome.Models.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyHome";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				ChangeHomeCommand.class,
				HomeCommand.class,
				HomeOfCommand.class,
				SetHomeCommand.class,
				DelHomeCommand.class,
				DelHomeOfCommand.class,
				DelAllHomeInWorldCommand.class
			};
		LISTENERS = null;
		CONFIG = Config.class;
	}
	
	@Override
	public void onEnable()
	{
		super.onEnable();
		HashMap<Integer, String> perms = Config.GetConfig().getMaxHomeRights();
		for(Integer key : perms.keySet())
		{
			Bukkit.getPluginManager().addPermission(new Permission(perms.get(key), "MyHome custom permission for maximum homes"));
		}
	}
	
	@Override
	protected void initDatabase() throws Exception
	{
		Home.CreateTable();
	}
}