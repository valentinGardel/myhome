package fr.minecraft.MyHome.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyHome.Models.Home;

public class HomeOfTabCompleter extends MinecraftTabCompleter
{
	
	@SuppressWarnings("deprecation")
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> homesName = new ArrayList<String>();
		if(sender instanceof Player)
		{
			switch(args.length)
			{
				case 1:
					homesName = null;
					break;
				case 2:
					OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
					List<Home> homes = Home.GetHomes(player.getUniqueId());

					for(Home h : homes)
					{
						if(h.nom.toUpperCase().startsWith(args[1].toUpperCase()))
							homesName.add(h.nom);
					}
					break;
			}
		}
		return homesName;
	}

}
