package fr.minecraft.MyHome.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyHome.Models.Home;

public class HomeTabCompleter extends MinecraftTabCompleter
{
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> homesName = new ArrayList<String>();
		if(sender instanceof Player)
		{
			if(args.length==1)
			{
				Player player = (Player) sender;
				List<Home> homes = Home.GetHomes(player.getUniqueId());

				for(Home h : homes)
				{
					if(h.nom.toUpperCase().startsWith(args[0].toUpperCase()))
						homesName.add(h.nom);
				}
			}
		}
		return homesName;
	}

}
